class QR {
    constructor() {
        this.renderQR();
    }

    renderQR() {
        this.$qr = document.createElement('div');
        this.$qr.classList.add('qr');
        let cells = '';
        let isMarker;
        let param;

        for (let i = 0; i < 9; i++) {
            switch (true) {
                case i === 0 || i === 2 || i === 6:
                    param = [8, 9, 10, 11, 12, 15, 19, 22, 26, 29, 33, 36, 37, 38, 39, 40];
                    isMarker = true;
                    break;
                case i === 1:
                    param = [0, 7, 14, 21, 28, 35, 42, 6, 13, 20, 27, 34, 41, 48];
                    isMarker = false;
                    break;
                case i === 3:
                    param = [0, 1, 2, 3, 4, 5, 6, 42, 43, 44, 45, 46, 47, 48];
                    isMarker = false;
                    break;
                case i === 4:
                    param = [0, 6, 42];
                    isMarker = false;
                    break;
                case i === 5:
                    param = [0, 1, 2, 3, 4, 5, 6];
                    isMarker = false;
                    break;
                case i === 7:
                    param = [0, 7, 14, 21, 28, 35, 42];
                    isMarker = false;
                    break;
                default:
                    param = [];
                    isMarker = false;
            }

            cells += QR.renderCells(isMarker, param);
        }

        this.$qr.innerHTML = cells;

        document.body.appendChild(this.$qr);

        this.paintCells();
    }

    static renderCells(isMarker, arr) {
        let resultStr = '';

        for (let i = 0; i < 49; i++) {
            const fill = !arr.includes(i);
            let className = 'qr__cell--small';

            if (!isMarker && fill) {
                className += ' js_paint';
            }

            resultStr += QR.renderCell({
                className,
                fill
            });
        }

        return QR.renderCell({
            className: 'qr__cell',
            children: resultStr
        });
    }

    static renderCell(obj) {
        const children = obj.children ? obj.children : '';
        const className = obj.className ? obj.className : '';
        const style = obj.fill ? 'style="background-color: var(--color)"' : '';

        return `<div class="${className}" ${style}>${children}</div>`;
    }

    paintCells() {
        [...document.querySelectorAll('.js_paint')].forEach((i) => {
            QR.paint(i);
        });
    }

    static paint(el) {
        if (QR.rnd()) {
            if (el.getAttribute('style')) {
                el.removeAttribute('style');
            } else {
                el.style.backgroundColor = 'var(--color)';
            }
        }
    }

    static rnd() {
        return Math.random() >= 0.5;
    }
}

const qr = new QR();